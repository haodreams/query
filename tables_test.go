/*
 * @Author: Wangjun
 * @Date: 2021-05-27 15:06:09
 * @LastEditTime: 2022-10-27 13:12:11
 * @LastEditors: Wangjun
 * @Description:
 * @FilePath: \query\tables_test.go
 * hnxr
 */

package query

import (
	"log"
	"testing"

	"github.com/stretchr/testify/assert"
)

type Value struct {
	AV float64
	C6 float64
}

type Fan struct {
	ID  int
	Val Value
}

func TestArray(t *testing.T) {
	fans := make([]*Fan, 3)
	fans[0] = new(Fan)
	fans[0].ID = 100
	fans[1] = new(Fan)
	array, err := NewTable(fans)
	if err != nil {
		log.Println(err)
	}
	//log.Printf("%v %v", array, err)
	array, err = array.EQ("ID", 100, true)
	log.Printf("%v %v", array, err)
}

func TestCol(t *testing.T) {
	fans := make([]*Fan, 3)
	fans[0] = new(Fan)
	fans[0].ID = 100
	fans[0].Val.AV = 95
	fans[0].Val.C6 = 100

	tab, err := NewTable(fans)
	if err != nil {
		log.Println(err)
		return
	}

	RegisterTable("plant", tab)
	schema, err := NewFastSchema(`FROM("plant").EQ("ID",100).Sum("Val.C6").Mult(0.95).ColSub("Val.AV").IfGT(0,1,2)`)

	assert.NoError(t, err)

	v, err := schema.Eval()
	assert.NoError(t, err)
	assert.Equal(t, v, 2.0, "数据异常1")

	fans[0].Val.AV = 94
	fans[0].Val.C6 = 100
	//schema, err = NewFastSchema(`FROM("plant").EQ("ID",100).Sum("C6").Mult(0.95)`)
	assert.NoError(t, err)
	v, err = schema.Eval()
	assert.NoError(t, err)
	log.Println(v)
	assert.Equal(t, v, 1.0, "数据异常2")
}
