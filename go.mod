module gitee.com/haodreams/query

go 1.19

replace gitee.com/haodreams/golib => ../../haodreams/golib

require (
	gitee.com/haodreams/golib v0.0.0-00010101000000-000000000000
	gitee.com/haodreams/libs v0.0.0-20230316154901-c15b7c533e6c
	github.com/alecthomas/participle/v2 v2.0.0-beta.5
	github.com/stretchr/testify v1.8.2
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	go.uber.org/atomic v1.10.0 // indirect
	go.uber.org/multierr v1.8.0 // indirect
	go.uber.org/zap v1.24.0 // indirect
	gopkg.in/natefinch/lumberjack.v2 v2.2.1 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
