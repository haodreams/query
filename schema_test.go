/*
 * @Author: Wangjun
 * @Date: 2021-06-08 13:17:00
 * @LastEditTime: 2023-03-10 20:03:26
 * @LastEditors: wangjun haodreams@163.com
 * @Description: 测试
 * @FilePath: \query\schema_test.go
 * hnxr
 */

package query

import (
	"fmt"
	"log"
	"testing"

	"github.com/alecthomas/participle/v2"
	"github.com/stretchr/testify/assert"
)

type T struct {
	A1 float64
	A2 float64
	A3 float64
}

func TestColAdd(t *testing.T) {
	ts := make([]*T, 1)
	r := new(T)
	r.A1 = 100
	r.A2 = 200
	r.A3 = 300
	ts[0] = r
	tab, err := NewTable(ts)
	log.Println(err)
	RegisterTable("t", tab)
	qstr := `FROM("t").ColSum("A1","A2","A3")`
	s, err := NewSchema(qstr)
	if err != nil {
		log.Fatal(err)
	}
	val, err := s.Eval()
	if err != nil {
		log.Fatal(err)
	}
	log.Println(val)
}

func TestColGtCount(t *testing.T) {
	ts := make([]*T, 1)
	r := new(T)
	r.A1 = 100
	r.A2 = 200
	r.A3 = 300
	ts[0] = r
	tab, err := NewTable(ts)
	log.Println(err)
	RegisterTable("t", tab)
	qstr := `FROM("t").ColGtCount(150,"A1","A2","A3")`
	s, err := NewSchema(qstr)
	if err != nil {
		log.Fatal(err)
	}
	val, err := s.Eval()
	if err != nil {
		log.Fatal(err)
	}
	log.Println(val)
}

// 测试IF 函数
func TestIf(t *testing.T) {
	ts := make([]*T, 1)
	r := new(T)
	r.A1 = 100
	r.A2 = 200
	r.A3 = 300
	ts[0] = r
	tab, err := NewTable(ts)
	log.Println(err)
	RegisterTable("t", tab)
	qstr := `FROM("t").Count("*").IfGT(1,1,5)`
	s, err := NewSchema(qstr)
	if err != nil {
		log.Fatal(err)
	}
	val, err := s.Eval()
	if err != nil {
		log.Fatal(err)
	}
	log.Println(val)
}

func TestIfCount(t *testing.T) {
	ts := make([]*T, 1)
	r := new(T)
	r.A1 = 100
	r.A2 = 200
	r.A3 = 300
	ts[0] = r
	tab, err := NewTable(ts)
	log.Println(err)
	RegisterTable("t", tab)
	qstr := `FROM("t").ColGtCount(200,"A1","A2","A3").IfGT(0,1,5)`
	s, err := NewSchema(qstr)
	if err != nil {
		log.Fatal(err)
	}
	val, err := s.Eval()
	if err != nil {
		log.Fatal(err)
	}
	log.Println(val)
}

func TestSchema(t *testing.T) {
	qstr := `FROM("es").LT("Genpower.Value",0).GT("Genpower.Value",-5000).Sum("Genpower.Value")`
	var parse = participle.MustBuild[Schema]()
	q, err := parse.ParseString("", qstr)
	if err != nil {
		log.Fatalln(err)
	}
	log.Println("table:", q.Table)
	for _, f := range q.Funcs {
		if f == nil {
			continue
		}
		log.Println(f)
	}
}

// 测试IN的方法
func TestIN(t *testing.T) {
	fans := make([]*Fan, 3)
	fans[0] = new(Fan)
	fans[0].ID = 100
	fans[0].Val.AV = 100
	fans[1] = new(Fan)
	fans[1].ID = 101
	fans[1].Val.AV = 200
	fans[2] = new(Fan)
	fans[2].ID = 102
	fans[2].Val.AV = 300
	array, err := NewTable(fans)
	if err != nil {
		log.Println(err)
	}

	RegisterTable("fan", array)

	qstr := `FROM("fan").IN("ID",100,101,102).Avg("Val.AV")`
	s, err := NewSchema(qstr)
	if err != nil {
		log.Fatal(err)
	}
	val, err := s.Eval()
	if err != nil {
		log.Fatal(err)
	}

	assert.Equal(t, val, 200.0, "结果错误")
	log.Printf("%v", val)
}

// func TestSchema2(t *testing.T) {
// 	qstr := `FROM("es").LT("Genpower.Value",0).GT("Genpower.Value",-5000).ColAdd("Genpower.Value","Genpower.Value1","Genpower.Value2").IfGT("this",1,5)`
// 	var parse = participle.MustBuild(&Schema{})
// 	q := new(Schema)
// 	err := parse.ParseString("", qstr, q)
// 	if err != nil {
// 		log.Fatalln(err)
// 	}
// 	log.Println("table:", q.Table)
// 	for _, f := range q.Funcs {
// 		if f == nil {
// 			continue
// 		}
// 		log.Println(f)
// 	}
// }

func TestObj(t *testing.T) {
	q1 := new(Table)
	q2 := new(Table)
	f1 := q1.Max
	f2 := q2.Max
	fmt.Println(f1("a"))
	fmt.Println(f2("b"))
}

func test(b *[]byte) {
	*b = make([]byte, 5)
}

func TestA(t *testing.T) {
	a := make([]byte, 1)
	fmt.Println(len(a))
	test(&a)
	b := &a
	fmt.Println(len(a))
	fmt.Printf("%p %p", &a, b)
}
